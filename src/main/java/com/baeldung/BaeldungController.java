package com.baeldung;

import com.baeldung.remote.RemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.rmi.Remote;

@RestController
public class BaeldungController {

    @Autowired
    private RemoteService remoteService;

    @GetMapping("/hello")
    public String sayHello(HttpServletResponse response) {
        return remoteService.getHello() + ":arthur";
    }

    @PostMapping("/baeldung")
    public String sayHelloPost(HttpServletResponse response) {
        return "hello";
    }
}
