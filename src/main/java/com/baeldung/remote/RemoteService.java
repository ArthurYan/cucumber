package com.baeldung.remote;

import org.springframework.stereotype.Service;

@Service("remoteService")
public class RemoteService {

    public String getHello() {
        return "hello arthur";
    }

}
