package com.baeldung;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.HashMap;
import java.util.Map;

public class TestDoubleInjector implements BeanPostProcessor {

    private static Map<String, Object> MOCKS = new HashMap<String, Object>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        if (MOCKS.containsKey(beanName)) {
            return MOCKS.get(beanName);
        }
        return bean;
    }

    public void addMock(String beanName, Object mock) {
        MOCKS.put(beanName, mock);
    }

    public void clear() {
        MOCKS.clear();
    }

}