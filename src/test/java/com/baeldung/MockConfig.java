package com.baeldung;

import com.baeldung.remote.RemoteService;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class MockConfig {

    @Bean
    public RemoteService remoteService() {
        return Mockito.mock(RemoteService.class);
    }
}
